import './style.css'
import javascriptLogo from './javascript.svg'
import { setupCounter } from './counter.js'
import * as THREE from 'three'
import { Plane, PointLight } from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';

const fbxLoader = new FBXLoader();

const gltfloader = new GLTFLoader();

window.onbeforeunload = function () {
  window.scrollTo(0, 0);
}

function LoadFBX(path,x ,y ,z, scale){
  return new Promise( (callback)=>
  fbxLoader.load(path, (fbxobject)=>{
    fbxobject.scale.set(scale,scale,scale)
    fbxobject.position.setZ(z);
    fbxobject.position.setX(x);
    fbxobject.position.setY(y);
    scene.add(fbxobject);
    callback(fbxobject);
  } )
  )
}


function LoadGLTF(path,x ,y ,z, scale, rotationX, rotationY, rotationZ){
  return new Promise( (callback)=>
    gltfloader.load(path, (gltfObject)=>{
      gltfObject.scene.scale.set(scale,scale,scale)
      gltfObject.scene.position.setZ(z);
      gltfObject.scene.position.setX(x);
      gltfObject.scene.position.setY(y);
      
      
      
      scene.add(gltfObject.scene);
      callback(gltfObject);
    } )
  )
  
}

let mixer = new THREE.AnimationMixer();
let modelReady = false
const gltfLoaderAnim = new GLTFLoader()


gltfLoaderAnim.load(
  './assets/rower_animation/scene.gltf',
  (gltf) => {
      gltf.scene.scale.set(0.02, 0.02, 0.02)

      mixer = new THREE.AnimationMixer(gltf.scene)

      const animationAction = mixer.clipAction((gltf).animations[0])
      gltf.scene.position.setX(-10);
      gltf.scene.position.setY(-2);
      gltf.scene.position.setZ(-12);
      gltf.scene.rotation.y =95;
      
      scene.add(gltf.scene)
      animationAction.play()
      
      modelReady=true;
      
  }
)




let platformPath = './assets/Platforme/GroundCubeQuad 1.fbx';

const scene = new THREE.Scene();

const camera =new THREE.PerspectiveCamera(75,window.innerWidth/window.innerHeight,0.1,1000);





const renderer = new THREE.WebGL1Renderer({
  canvas: document.querySelector('#bg'),
});

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);





const geometry = new THREE.TorusGeometry(3,1);
const material = new THREE.MeshStandardMaterial({color: 0x2596be });
const Tshape = new THREE.Mesh(geometry,material,1);

scene.add(Tshape);

const pointLight = new THREE.PointLight(0xffffff);
pointLight.position.set(2,2,2);

const ambiantLight = new THREE.AmbientLight(0xebbdcf);
scene.add(ambiantLight, pointLight);

const controls = new OrbitControls(camera, renderer.domElement);


const profileTexture = new THREE.TextureLoader().load("background.jpg" ,undefined,undefined,(err)=>{console.log("problemos")});



const me = new THREE.Mesh(
  new THREE.BoxGeometry(2,2,2),
  new THREE.MeshBasicMaterial({map : profileTexture})
)


const project1 = new THREE.Mesh(
  new THREE.BoxGeometry(2,2,2),
  new THREE.MeshBasicMaterial({color:"black"})
)
const project2 = new THREE.Mesh(
  new THREE.BoxGeometry(2,2,2),
  new THREE.MeshBasicMaterial({color:"blue"})
)

const project3 = new THREE.Mesh(
  new THREE.BoxGeometry(2,2,2),
  new THREE.MeshBasicMaterial({color:"green"})
)
const project4 = new THREE.Mesh(
  new THREE.BoxGeometry(2,2,2),
  new THREE.MeshBasicMaterial({color:0xfffff})
)

const focus = new THREE.Mesh(
  new THREE.BoxGeometry(0,0,0),
)

/*const gridHelper = new THREE.GridHelper(10, 10, 0xaec6cf, 0xaec6cf)
scene.add(gridHelper)*/


me.position.setZ(0) ;
me.position.setX(0);

project1.position.setZ(-10);
project1.position.setX(10);
project1.position.setY(-2.9);



LoadGLTF("./assets/low_poly_planet_earth/scene.gltf",100,-150,-55,60).then(result=>{
  setInterval(()=>animateEarth(result),25)
})
LoadGLTF("./assets/low_poly_moon/scene.gltf",-100,5,55,20)

LoadFBX(platformPath,project1.position.x,-2,project1.position.z,0.01)
LoadGLTF("./assets/rick_rick_and_morty/scene.gltf",project1.position.x-2.5,-2,project1.position.z-2.5,1.2).then(result => {
  result.scene.rotateY(45);
})
LoadGLTF("./assets/corona_virus/scene.gltf",project1.position.x+2,0,project1.position.z+2,2).then(result => {

  
})

LoadGLTF("./assets/flag/scene.gltf",project1.position.x+2,-2,project1.position.z-2,2)


project2.position.setZ(-10) ;
project2.position.setX(-10);
project2.position.setY(-2.9);
LoadFBX(platformPath,project2.position.x,-2,project2.position.z,0.01)
//LoadGLTF("./assets/first_aid_kit/scene.gltf",project2.position.x-2,-1.5,project2.position.z+0.5,0.3)
//LoadGLTF("./assets/male_jogging_30_frames_loop/scene.gltf",project2.position.x,-1,project2.position.z-2,2.5)
LoadGLTF("./assets/oculus_rift_cv1/scene.gltf",project2.position.x+1,0,project2.position.z+2.5,0.25).then(result => {
  result.scene.rotateY(-80);
})

LoadGLTF("./assets/robotic_hand/scene.gltf",project2.position.x-2,-0.5,project2.position.z+0.5,0.30).then(result => {
  result.scene.rotateZ(-90)
  setInterval(()=>animateObject(result),25);
})

/*const ReviVRTexture = new THREE.TextureLoader().load('ReviVRLogo.png')

const plane = new THREE.Mesh(
  new THREE.BoxGeometry(7,0.01,5),
  new THREE.MeshBasicMaterial({map:ReviVRTexture})
)

plane.position.setZ(project2.position.z);
plane.position.setX(project2.position.x-5);
plane.position.setY(project2.position.y+3);
plane.rotateY(1.6)
plane.rotateX(1.5)*/





 project3.position.setZ(10);
 project3.position.setX(-10);
 project3.position.setY(-2.9);

const P3Z = project3.position.z
const P3X =project3.position.x
const P3Y =project3.position.y
LoadFBX(platformPath,project3.position.x,-2,project3.position.z,0.01)
LoadGLTF("./assets/low_poly_mobile_phone/scene.gltf",project3.position.x,0,project3.position.z+2,7).then(result => {
  result.scene.rotateY(0);
})
const cylinder = new THREE.Mesh(
  new THREE.CylinderGeometry(0.5,0.5,1),
  new THREE.MeshBasicMaterial({color:"Yellow"})
)
cylinder.position.setZ(P3Z-2);
cylinder.position.setX(P3X);
cylinder.position.setY(P3Y+2);

const cone = new THREE.Mesh(
  new THREE.ConeGeometry(0.5,1,10),
  new THREE.MeshBasicMaterial({color:"Red"})
)
cone.position.setZ(P3Z);
cone.position.setX(P3X-1.5);
cone.position.setY(P3Y+2);

const cube = new THREE.Mesh(
  new THREE.BoxGeometry(0.5,0.5,0.5),
  new THREE.MeshBasicMaterial({color:"blue"})
)
cube.position.setZ(P3Z);
cube.position.setX(P3X+2);
cube.position.setY(P3Y+2);

scene.add(cylinder,cone,cube)

project4.position.setZ(10);
project4.position.setX(10);
project4.position.setY(-2.9);
LoadFBX(platformPath,project4.position.x,-2,project4.position.z,0.01)
LoadGLTF("./assets/low_poly_castle/scene.gltf",project4.position.x,-1.5,project4.position.z-2,0.0008).then(result => {
  result.scene.rotateY(-45)
})
LoadGLTF("./assets/unreal_engine_cglogo/scene.gltf",project4.position.x+1,0,project4.position.z+2,0.01).then(result => {
  result.scene.rotateY(-90);
})

//const UnrealProjectTexture = new THREE.TextureLoader().load('UE5Project.jpg')
/*const planeP4 = new THREE.Mesh(
  new THREE.BoxGeometry(10,0.01,5),
  new THREE.MeshBasicMaterial({map:UnrealProjectTexture})
)

planeP4.position.setZ(project4.position.z);
planeP4.position.setX(project4.position.x+4);
planeP4.position.setY(project4.position.y+3);
planeP4.rotateZ(1.5)
planeP4.rotateY(-1.55)*/




focus.position.setZ(-20);
focus.position.setX(0);
focus.position.setY(-20);







scene.add(me,project1,project2,project3,project4,focus);

function addStars(){
  const geometry = new THREE.SphereGeometry(0.5,24,24);
  const material = new THREE.MeshStandardMaterial({color: 0xffffff});
  const star = new THREE.Mesh(geometry,material);
  const[x,y,z] = Array(3).fill().map(()=> THREE.MathUtils.randFloatSpread(1000));
  star.position.set(x,y,z);
  scene.add(star);
}

function addTshapes(){
  const geometry = new THREE.TetrahedronGeometry(1,0);
  const color = THREE.MathUtils.randInt(0,0xffffff);
  const material = new THREE.MeshStandardMaterial({color: color});
  const Tshape = new THREE.Mesh(geometry,material);
  const[x,y,z] = Array(3).fill().map(()=> THREE.MathUtils.randFloatSpread(1000));
  Tshape.position.set(x,y,z);
  scene.add(Tshape);
  
  return Tshape;
}

Array(1000).fill().forEach(addStars);

scene.background = new THREE.Color(0x111122);






window.addEventListener('resize', onWindowResize, false)
function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight)
    renderer.render(scene,camera);
}

function lerp(x, y, a) {
  return (1 - a) * x + a * y
}

function scalePercent(start, end) {
  return (scrollPercent - start) / (end - start)
}


/*camera.position.setX(-25)
camera.position.setY(25)
camera.position.setZ(0)*/
const animationScripts  = []


//add an animation that moves the cube through first 40 percent of scroll
animationScripts.push({
  start: 0,
  end: 8,
  func: () => {
      camera.lookAt(focus.position)
      focus.position.z = lerp(-10, 0, scalePercent(0, 8))
      focus.position.y = lerp(-10, 0, scalePercent(0, 8))
      camera.position.z= lerp(0, 10, scalePercent(0, 8))
      camera.position.y= lerp(27, 0, scalePercent(0, 8))
      camera.position.x= lerp(-27, 0, scalePercent(0, 8))
      //console.log("jui la " + camera.position.x +"X "+ camera.position.z+"Z")      
      
  },
})

animationScripts.push({
  start: 8,
  end: 15.49,
  func: () => {
    camera.lookAt(focus.position)
    focus.position.x = lerp(0, -7, scalePercent(8, 15.49))
    camera.position.y= lerp(2, 5, scalePercent(8, 15.49))
    camera.position.z= lerp(9, 10, scalePercent(8, 15.49))
    //console.log("jui la " + camera.position.x +"X "+ camera.position.z+"Z")
  },
})

animationScripts.push({
  start: 15.5,
  end: 23,
  func: () => {
    camera.lookAt(focus.position)
    focus.position.x = lerp(-7, project1.position.x, scalePercent(15.5, 23))
    focus.position.y = lerp(0, project1.position.y, scalePercent(15.5, 23))
    focus.position.z = lerp(0, project1.position.z, scalePercent(15.5, 23))
    camera.position.z= lerp(10, -15, scalePercent(15.5, 23)) 
  },
})


animationScripts.push({
  start: 23,
  end: 33,
  func: () => {
    camera.lookAt(focus.position)
    focus.position.z = lerp(project1.position.z,project1.position.z+10 , scalePercent(23, 33))
    focus.position.y = lerp(project1.position.y,project1.position.y-2 , scalePercent(23, 33))
    focus.position.x = lerp(project1.position.x, project1.position.x-8, scalePercent(23, 33))
    //camera.position.x = lerp(camera.position.x,0.5 , scalePercent(23, 33))
    
  },
})


animationScripts.push({
  start: 33,
  end: 36,
  func: () => {
    camera.lookAt(focus.position)
    focus.position.z = lerp(project1.position.z+10, project2.position.z, scalePercent(33, 36))
    focus.position.y = lerp(project1.position.y-2, project2.position.y, scalePercent(33, 36))
    focus.position.x = lerp(project1.position.x-8, project2.position.x, scalePercent(33, 36))
    
  },
})

animationScripts.push({
  start: 36,
  end: 48,
  func: () => {
    camera.lookAt(focus.position)
    focus.position.y = lerp(project2.position.y, project2.position.y-2, scalePercent(36, 48))
    focus.position.z = lerp(project2.position.z, project2.position.z+8, scalePercent(36, 48))
    
  },
})

animationScripts.push({
  start: 48,
  end: 53,
  func: () => {
    camera.lookAt(focus.position)
    focus.position.x = lerp(project2.position.x, project3.position.x, scalePercent(48, 53))
    focus.position.y = lerp(project2.position.y-2, project3.position.y, scalePercent(48, 53))
    focus.position.z = lerp(project2.position.z+10, project3.position.z, scalePercent(48, 53))
    //console.log(camera.position.z + "Z" + camera.position.x + "Y" )
    
  },
})

animationScripts.push({
  start: 53,
  end: 56,
  func: () => {
    camera.lookAt(focus.position)
    camera.position.z =lerp(-15, 0, scalePercent(53, 56))
    camera.position.x =lerp(0.96502, -5, scalePercent(53, 56))
    //console.log(camera.position.z + "Z" + camera.position.x + "X" )
    focus.position.x = lerp(project2.position.x, project3.position.x-10, scalePercent(53, 56))


  },
})

animationScripts.push({
  start: 56,
  end: 63,
  func: () => {
    camera.lookAt(focus.position)
    camera.position.z =lerp(0.13, 0, scalePercent(56, 63))
    camera.position.x =lerp(-5, -10, scalePercent(56, 63))

  },
})

animationScripts.push({
  start: 63,
  end: 68,
  func: () => {
    camera.lookAt(focus.position)
    focus.position.x = lerp(project3.position.x-10, project4.position.x, scalePercent(63, 68))

  },
})

animationScripts.push({
  start: 68,
  end: 70,
  func: () => {
    camera.lookAt(focus.position)
    camera.position.z =lerp(0, 8, scalePercent(68, 70))
    camera.position.x =lerp(-10, 0, scalePercent(68, 70))
    //console.log("jui la " + camera.position.x +"X "+ camera.position.z+"Z")


  },
})

animationScripts.push({
  start: 70,
  end: 72,
  func: () => {
    camera.lookAt(focus.position)
    focus.position.z = lerp(project4.position.z, project4.position.z-12, scalePercent(70, 71))
    //console.log("jui la " + camera.position.x +"X "+ camera.position.z+"Z")


  },
})

animationScripts.push({
  start: 72,
  end: 84,
  func: () => {
    camera.lookAt(focus.position)
    focus.position.z = lerp(project4.position.z-12, 0, scalePercent(72, 84))
    focus.position.x = lerp(project4.position.x, 0, scalePercent(72, 84))
  },
})

animationScripts.push({
  start: 84,
  end: 110,
  func: () => {
    camera.lookAt(me.position)
    camera.position.z =lerp(8, 4, scalePercent(83, 100))
    camera.position.x =lerp(0, 0, scalePercent(83, 100))
    camera.position.y =lerp(0, 30, scalePercent(83, 100))
    //console.log("jui la " + camera.position.x +"X "+ camera.position.z+"Z")
    
  },
})



//console.log(animationScripts[0])

function playScrollAnimations() {
  animationScripts.forEach((a) => {
      if (scrollPercent >= a.start && scrollPercent < a.end) {
          a.func()
      }
  })
}









let scrollPercent = 0

document.body.onscroll = () => {
    // cross pruct to calculate the current scroll progress as a percentage
    scrollPercent =
        ((document.documentElement.scrollTop)*100 / ((document.documentElement.scrollHeight) - document.documentElement.clientHeight)) ;
        //console.log("scroll percent" + scrollPercent);
        /*console.log("scroll top" + document.documentElement.scrollTop);
        console.log("scroll height" + document.documentElement.scrollHeight);
        console.log("scroll client" + document.documentElement.clientHeight);*/
        
}

function animateObject(object){
  
  object.scene.rotateY(0.01)
  object.scene.rotateZ(0.01)

}

function animateEarth(object){
  
  object.scene.rotateY(0.0003)
  

}
const clock = new THREE.Clock()

function animate(){
  requestAnimationFrame(animate);
  renderer.render(scene,camera);
  controls.update();
  Tshape.rotation.x +=0.01;
  me.rotation.y+=0.01
  playScrollAnimations()
  if (modelReady) mixer.update(clock.getDelta())
}

animate();