Model Information:
* title:	unreal engine CGLogo
* source:	https://sketchfab.com/3d-models/unreal-engine-cglogo-4fee7cf36bdb4cf0929a060a7ca27181
* author:	moorish (https://sketchfab.com/moorish)

Model License:
* license type:	CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)
* requirements:	Author must be credited. Commercial use is allowed.

If you use this 3D model in your project be sure to copy paste this credit wherever you share it:
This work is based on "unreal engine CGLogo" (https://sketchfab.com/3d-models/unreal-engine-cglogo-4fee7cf36bdb4cf0929a060a7ca27181) by moorish (https://sketchfab.com/moorish) licensed under CC-BY-4.0 (http://creativecommons.org/licenses/by/4.0/)